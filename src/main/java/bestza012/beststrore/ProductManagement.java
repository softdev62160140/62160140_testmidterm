/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bestza012.beststrore;

import static bestza012.beststrore.ProductManagement.productList;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author WIN10
 */
public class ProductManagement  implements Serializable{

    static ArrayList<Product> productList = new ArrayList<>();

    static {
        load();
    }

    //CRUD
    //Create
    static boolean addProduct(Product product) {
        productList.add(product);
        save();
        return true;
    }

    //Delete
    static boolean deleteProduct(Product product) {
        productList.remove(product);
        save();
        return true;
    }

    //Delete index
    static boolean deleteProduct(int index) {
        productList.remove(index);
        save();
        return true;
    }

    //Update
    static boolean updateProduct(int index, Product product) {
        productList.set(index, product);
        save();
        return true;
    }

    static Product getProduct(int index) {
        return productList.get(index);
    }

    static ArrayList<Product> getProduct() {
        return productList;
    }

    static boolean isEmtry() {
        if (productList.isEmpty()) {

            return true;
        }
        return false;
    }

    static void clear() {
        productList.clear();
        save();
    }

    static int totalItem() {
        int sumtotalItem = 0;

        for (int i = 0; i < productList.size(); i++) {
            sumtotalItem += productList.get(i).getAmount();
        }

        return sumtotalItem;
    }

    static double totalPrice() {

        double sumprice = 0.00;

        for (int i = 0; i < productList.size(); i++) {
            sumprice += productList.get(i).getAmount() * productList.get(i).getPrice();
        }

        return sumprice;
    }

    static void save() {
        File file = null;
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;

        try {
            file = new File("Best.dat");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(productList);
            fos.close();
            oos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ProductManagement.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ProductManagement.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    static void load() {
        File file = null;
        FileInputStream fis = null;
        ObjectInputStream ois = null;

        try {
            file = new File("Best.dat");
            fis = new FileInputStream(file);
            ois = new ObjectInputStream(fis);
            productList = (ArrayList<Product>) ois.readObject();
            ois.close();
            fis.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ProductManagement.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ProductManagement.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ProductManagement.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
